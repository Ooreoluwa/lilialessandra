import requests
import xmltodict
import country_converter
import json
import logging
import os
from dateutil import parser
from facebook_business.api import FacebookAdsApi
from facebook_business.adobjects.serverside import (
    user_data,
    custom_data,
    event,
    event_request,
    action_source,
    content
)

logging.basicConfig(
    format="%(asctime)s %(levelname)s: %(message)s",
    level=os.environ.get("LOG_LEVEL", "INFO"),
)

code = 1

FB_ACCESS_TOKEN = os.environ.get("FB_ACCESS_TOKEN",
                                 "EAACZBQdYXFngBAP1GAw0ldwMlGJCM5XuSJdCzuFuE8MO8iwjcSDN"
                                 "mgRNW73jpq5k9q5PmpsdmVClBRI3Jy7ugomFUuc8AUxwbu0kolrd5j"
                                 "KY49cpWFroE4ZAVZAoJuO72AyTNbkNX9VGnBpQU5Ap5Xmrcdw7njAS"
                                 "RUcp4R3cVLeD9R5FtkI")

FB_PIXEL_ID = os.environ.get(
    "FB_PIXEL_ID", "1654949371472988")  # LiliAlessandra.com Pixel
FB_TEST_CODE = os.environ.get("FB_TEST_CODE")
FB_EVENT_NAME = os.environ.get("FB_EVENT_NAME", "Purchase")
EXPORT_URL = os.environ.get("EXPORT_URL")

FacebookAdsApi.init(access_token=FB_ACCESS_TOKEN, debug=True)


def validate_fb_event(event_obj: event.Event) -> None:
    """
    Checks the the FB event object is valid.
    - Has at least one user data parameter defined.
    - Can be normalised successfully
    :param event_obj:
    :raises Exception: If invalid
    :return: None
    """
    logging.debug(f"Validating {event_obj.event_id}")
    normalised_event = event_obj.normalize()  # exception raised if unsuccessful
    if not normalised_event["user_data"]:
        # user_data is an empty dict so raise exception
        raise ValueError("Event does not have any user data")


def convert_to_json(xml_info):
    """
    Converts xml response to json dict
    Args:
        xml_data: the xml response information

    Returns: dict containing payload of customers' info
    """
    json_info = (json.loads(json.dumps(xmltodict.parse(xml_info))))
    return json_info


def get_info_from_lilialessandra():
    """
    Make a request to lilialessandra api to get request
    Args:
        None

    Returns: list: containing dicts of customer's info
    """
    logging.info("GETTING CUSTOMER INFO FROM LILIALESSANDRA")
    response = requests.get(
        EXPORT_URL,
        headers={"Accept": "application/json"}
    )
    logging.info(
        f"HEADER RESPONSE FROM ENDPOINT: {response.headers}")
    json_payload = convert_to_json(response.content)["xmldata"]
    logging.info(
        f"JSON RESPONSE FROM ENDPOINT: {json_payload}")
    if response.ok and json_payload:
        if type(json_payload["Orders"]) == list:
            return json_payload["Orders"]
        return [json_payload["Orders"]]
    return None


def send_event_to_fb(payload, headers):
    """
    Extract data from payload, create FB and send appropriate FB event object
    Ensures the sanity of the object sent to FB!
    :param payload:
    :param headers:
    :return:
    """
    logging.debug(
        "PURCHASE OBJECTS RECEIVED. EXTRACTING REQUIRED FIELDS FOR FB EVENT")

    logging.info(payload)

    if payload.get("OrderStatus") != "Processing":
        logging.info(
            "SKIPPING EVENT BECAUSE ORDER ID IS OF A DUPLICATED INSTANCE")
        return
    # checks if phonenumber contains at least one letter
    if (payload.get("BillingPhoneNumber")) and (payload.get("BillingPhoneNumber").upper() == payload.get("BillingPhoneNumber").lower()):
        phonenumber = payload.get("BillingPhoneNumber").replace(".", "")
    else:
        phonenumber = ""

    try:
        countrycode = country_converter.convert(
            names=payload.get("BillingCountry"), to="ISO2")
    except:
        countrycode = ""

    event_user_data = user_data.UserData(
        phone=phonenumber,
        last_name=payload.get("BillingLastName"),
        first_name=payload.get("BillingFirstName"),
        city=payload.get("BillingCity"),
        state=payload.get("BillingState"),  # change from sign to Full name
        country_code=countrycode,
        zip_code=payload.get("BillingPostalCode"),
    )

    if type(payload["OrderDetails"]) == list:
        OrderDetails = payload["OrderDetails"]
    else:
        OrderDetails = [payload["OrderDetails"]]

    logging.debug(OrderDetails)

    event_custom_data = custom_data.CustomData(
        value=float(payload["PaymentAmount"]),
        currency="USD",
        content_type='product_group',
        contents=[content.Content(
            product_id=order["ProductID"],
            quantity=order['Quantity'],
            item_price=order['ProductPrice'],
            title=order["ProductName"]
        ) for order in OrderDetails],
    )

    fb_event = event.Event(
        event_name=FB_EVENT_NAME,
        event_time=int(parser.parse(payload["OrderDate"]).timestamp()),
        event_source_url="http://www.lilialessandra.com/net/WebService.aspx",
        user_data=event_user_data,
        custom_data=event_custom_data,
        event_id=payload["OrderID"],
        action_source=action_source.ActionSource.OTHER
    )

    fb_event_request = event_request.EventRequest(
        events=[fb_event],
        pixel_id=FB_PIXEL_ID,
    )

    try:
        validate_fb_event(fb_event)
        logging.info(f"FB events created: {fb_event.to_dict()}")
        if FB_TEST_CODE:
            fb_event_request.test_event_code = FB_TEST_CODE
            logging.warning("SENDING AS TEST EVENT TO FB")
        response = fb_event_request.execute()
        logging.info(response.to_dict())
    except TypeError as exc:
        logging.warning(f"{repr(exc)} skipping")
    except ValueError as exc2:
        logging.warning(
            f"{repr(exc2)} skipping. Email was probably in the wrong format")


def main(request):
    """
    Responds to any HTTP request.
    Args:
        request (flask.Request): HTTP request object.
    Returns:
        The response text or any set of values that can be turned into a
        Response object using
        `make_response <http://flask.pocoo.org/docs/1.0/api/#flask.Flask.make_response>`.
    """

    if request.method == 'POST':
        json_payload = get_info_from_lilialessandra()
        if json_payload:
            for order in json_payload:
                send_event_to_fb(payload=order, headers=request.headers)
        else:
            logging.info("PAYLOAD EMPTY. SKIPPING")
        return "OK", 200, {'Access-Control-Allow-Origin': '*', }
    else:
        return "Error", 400, {'Access-Control-Allow-Origin': '*', }
