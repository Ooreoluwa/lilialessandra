### lilialessandra

Each directory is a cloud function handling a shopify event type which is translated to one or more FB event(s).

##### To deploy
Run commands from within the specific cloud functions folder.

So for deploying `lead` cloud fucntion the command will be 

```shell
gcloud --project lilialessandra-fb-capi functions deploy lead --entry-point main \
--runtime python38 --trigger-http --allow-unauthenticated --timeout 180 --ignore-file leadpayload.json
```

So for deploying `purchase` cloud fucntion the command will be 

```shell
gcloud --project lilialessandra-fb-capi functions deploy purchase --entry-point main \
--runtime python38 --trigger-http --allow-unauthenticated --timeout 180 --ignore-file order.json
```